package robots

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
	"unsafe"

	"github.com/bot-o-mat-Prosp3r/blog"
	"github.com/bot-o-mat-Prosp3r/database"
)

//Robot will hold struct data for robot
type Robot struct {
	ID           int64  `json:"id"`
	Userid       int64  `json:"userid"`
	Name         string `json:"name"`
	Type         string `json:"type"`
	Creationtime string `json:"creationtime,omitemtpy"`
}

//Robotskill will hold struct data for robot trained skill
type Robotskilled struct {
	Robottype string `json:"robottype"`
	Taskid    string `json:"taskid"`
}

//Robottypes holds the struct data for the type of robot
type Robottypes struct {
	Name string `json:"name"`
	//Skill string `json:"skill"`
}

//Tasktypes hold the struct data for robot task types
type Tasktypes struct {
	Description string  `json:"description"`
	Eta         float64 `json:"eta"`
}

//Enhancedtasktypes hold the struct data for robot task types
type Enhancedtasktypes struct {
	ID          string  `json:"id"`
	Description string  `json:"description"`
	Eta         float64 `json:"eta"`
}

//type Tasktypes struct {
//}

//User holds the struct data for user identity
type User struct {
	ID         int64  `json:"id"`
	Username   string `json:"username"`
	Email      string `json:"email"`
	Timejoined string `json:"timejoined,omitempty"`
	Status     string `json:"status"`
}

//Work will hold the struct data form Assined tasks
type Work struct {
	Jobid       int64   `json:"jobid"`
	Taskid      int64   `json:"taskid"`
	Botid       int64   `json:"botid"`
	Description string  `json:"description"`
	Tasketa     float64 `json:"tasketa"`
	Score       float64 `json:"score"`
	Starttime   string  `json:"starttime,omitempty"`
	Endtime     string  `json:"endtime,omitempty"`
	Status      string  `json:"status"`
}

//Robotx holds exported robot list of robots loaded into []Robot structs.
var Robotx []Robot

//Robotskill holds exported skilled/trained robot loaded into []Robotskilled struct
var Robotskill []Robotskilled

//Robottype holds exported list of Robot types struct
var Robottype []Robottypes

var tasktype []Tasktypes
var etasktype []Enhancedtasktypes
var user []User
var work []Work

//::::::::::::::::::::::::TO BE PRESET:::::::::::::::::::::::::::::::

//SetrobotTypes loads the robot breed from the robot types json file
func SetrobotTypes() error {
	//SET ALL ROBOT TYPES/BREED FROM JSON STORAGE FILE
	botFile := "robottypes.json"
	b, err := os.Open(botFile)
	if err != nil {
		_ = blog.Botlog("Failed opening bot types file: " + err.Error())
		return err
	}

	defer b.Close()

	bt, _ := ioutil.ReadAll(b)
	if err = json.Unmarshal(bt, &Robottype); err != nil {
		_ = blog.Botlog("Error Unmarshaling bot types : " + err.Error())
		return err
	}

	//SetRobotTypeSkills()
	//fmt.Println(Robottype)
	//fmt.Println(Robotskill)
	return nil
}

//SetSkillsFromTaskList SETS ALL ROBOT SKILLS TASK LIST FROM jSON STORAGE FILE
func SetSkillsFromTaskList() error {
	//load Skill file
	taskfile := "tasks.json"
	t, err := os.Open(taskfile)
	if err != nil {
		_ = blog.Botlog("Failed opening task file: " + err.Error())
		return err
	}
	defer t.Close()
	f, _ := ioutil.ReadAll(t)

	//Save the data to struct
	if err = json.Unmarshal(f, &tasktype); err != nil {
		_ = blog.Botlog("Error Unmarshalling task type file : " + err.Error())
		return err
	}
	//SET THE ENHANCED TASK TYPES
	SetTaskTypes() //ROBOT TASK TYPES

	//SetrobotTypes() //ROBOT TYPES
	return nil
}

//SetTaskTypes sets the task types and add an ID with the enhanced tasktypes struct
func SetTaskTypes() {
	//CREATE ENHANCED TASK TYPES WITH UNIQUEIDs
	count := 1
	for _, j := range tasktype {
		job := new(Enhancedtasktypes)
		if len(j.Description) > 0 {
			newid := strings.ReplaceAll(j.Description, " ", "-")
			//err = Scan(&)
			job.ID = newid
			job.Description = j.Description
			job.Eta = j.Eta
		}
		etasktype = append(etasktype, *job)
		count++
	}
	//fmt.Println(etasktype)
}

//getRandomBotSkillID scans through the list of enhanced task list and returns a random selected task ID
func getRandomBotSkillID() string {
	sizeOfTaskList := int(unsafe.Sizeof(etasktype)) //check the sze of the struct content

	countTaskType := 1
	//makeLimitedRandomInt(maxLen, maxNum, minNum)
	rn := makeLimitedRandomInt(1, sizeOfTaskList-1, 1) //a single element array
	for _, jobtype := range etasktype {
		if rn[0] == countTaskType {
			return jobtype.ID
		}
		countTaskType++
	}
	return ""
}

//SetRobotTypeSkills AKA train the bots sets/assigns special skills from the skills list to robot types
func SetRobotTypeSkills() {
	//GET THE BOT TYPE LIST
	//GET THE SKILLS LIST
	//RANDOMLY ASSING SKILLS
	//SET ROBOT TYPE SKILLS RANDOMLY FROM LIST OF TASKS
	count := 1
	for _, rb := range Robottype {
		skill := new(Robotskilled)

		skill.Robottype = rb.Name
		skill.Taskid = getRandomBotSkillID()
		for len(skill.Taskid) < 3 {
			//repeat if empty
			skill.Taskid = getRandomBotSkillID()
		}
		Robotskill = append(Robotskill, *skill)
		count++
	}
}

//::::::::::::::::::::::::::::::::::::::END TO BE PRESET DATA::::::::::::::::::::::::::::::::

//GetSkill will return given robot id's type skill id. usage
//usage a := Robotskill a.Getskill
func (t Robotskilled) GetSkill(robottype string) string {

	for _, rs := range Robotskill {
		if rs.Robottype == robottype {
			return rs.Taskid
		}
	}
	return ""
}

//CreateBot adds a new bot to the database
func CreateBot(botname, bottype string, userid int64) int64 {

	timecreated := time.Now()
	status := "C"
	//stmt, err := database.DB.Prepare("INSERT INTO users VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
	stmt, err := database.DB.Prepare("INSERT INTO bots VALUE(?, ?, ?, ?, ?, ?)")
	defer stmt.Close()
	result, err := stmt.Exec(0, botname, bottype, userid, timecreated, status)
	lastID, err := result.LastInsertId()
	if err != nil {
		log.Fatal(err)
		//fmt.Println(err.Error())
		_ = blog.Botlog("Line 312: " + err.Error())
	}
	//After creating a new robot, refresh the bot list struct.
	SetRobots()
	return lastID
}

//GetRobotInfo Returns a Robot struct with the information of the given robot id
func GetRobotInfo(botid int64) Robot {
	//SetRobots()
	for _, robos := range Robotx {
		if robos.ID == botid {
			newrob := robos
			return newrob
		}
	}
	return Robot{}
}

//AssignWork will assign work for a rebot
func AssignWork(robotid int64) {
	//SET ALL WORK IN WORK STRUCT

}

//WorkSpeed will Determine robots work speed percentage to the workload based on wether they have the skill or not
func WorkSpeed(botid int64, tasktype string) float64 {
	//thisrobot := GetRobotInfo(botid)

	return 100.00
}

func getRobotSkills() {}

func fetchRobotTypes() {}

//:::::::::::::::::::::::::::OPERATIONAL::::::::::::::::::::::::

//SetRobots pulls list of created robots from the database and loads them into the robot struct for easy access
func SetRobots() []Robot {
	//Clear the robot buffer
	Robotx = nil
	//Pull from database
	result, err := database.DB.Query("SELECT * FROM bots ORDER BY botid DESC")

	if err != nil {
		_ = blog.Botlog("Failed loading Robots from Database: " + err.Error())
		return nil
	}

	defer result.Close()

	for result.Next() {
		temprobot := new(Robot)
		err = result.Scan(&temprobot.ID, &temprobot.Name, &temprobot.Type, &temprobot.Userid, &temprobot.Creationtime)
		if err != nil {
			//fmt.Println(err.Error())
			_ = blog.Botlog("Failed loading Robots from Database: " + err.Error())
			return nil
		}
		Robotx = append(Robotx, *temprobot)
	}

	if err = result.Err(); err != nil {
		_ = blog.Botlog("Failed loading Robots from Database: " + err.Error())
		return nil
	}
	//Reutrn the struct
	return Robotx
}

//makeLimitedRandomNumber returns an array of randomly generated int numbers
//takes maximum length of array of numbers to be returned and generated within the bounds of maximumNuber and minimum number
func makeLimitedRandomInt(maxLen, maxNum, minNum int) []int {
	//var maxNum = 45
	//var maxLen = 7
	//seed for our rand functions
	rand.Seed(time.Now().UnixNano())

	//store jackP
	jackP := make([]int, maxLen) //create an empty slice of max number of characters same as number of numbers to be generated

	x := maxLen
	y := minNum
	for i := 0; i <= x; i++ {
		NewNum := rand.Intn(int(maxNum))
		//reset the range of slice index y to prevent index out of range error
		if y >= maxLen {
			y = 0
		}

		checker := checkP(jackP, int64(NewNum))
		if checker == false {
			jackP[y] = NewNum
			y++
		} else {
			//fmt.Printf("Got one duplicate: %v", NewNum )
		}
	}
	//fmt.Println(jackP)
	return jackP
}

//checkP checks if there's a duplicate of the given number in the slice
func checkP(jackP []int, NewNum int64) bool {
	isThere := false
	//fmt.Println(NewNum)
	for _, Numbs := range jackP {
		if NewNum == int64(Numbs) {
			isThere = true
		}
	}
	return isThere
}

//getExcitementWords give the robot a random emtotion word based on their excitment level
func getExcitementWords(excitementLevel float64) string {
	//Returns excitement level words from an array/dictionary of words based on how excited the robots are to do the work.

	var words [11]string
	words[0] = "for shizzle, I'm not built for this nonsense :( "
	words[1] = "Can't wait to clock out, I need me a samich and some cool-aid :("
	words[2] = "This is it I'm getting outta this (sh*&^$#ty) job after this :("
	words[3] = "I'm not being paid enough for this job :("
	words[4] = "This is getting to be a real JOB :("
	words[5] = "Sigh! I'll survive"
	words[6] = "I'm loving this Gig"
	words[7] = "This is a cake walk"
	words[8] = "I'm basically being paid to have fun :)"
	words[9] = "I can get used to this"
	words[10] = "Give me some real work next time, this is too easy"
	var wordx string

	if excitementLevel > 5 {
		//positive words
		minNum := 5
		maxLen := 1
		maxNum := 10
		theNum := makeLimitedRandomInt(maxLen, maxNum, minNum)
		//rand.Intn(min)
		wordx = words[theNum[0]]
	} else {
		//negative words
		minNum := 0
		maxLen := 1
		maxNum := 5
		theNum := makeLimitedRandomInt(maxLen, maxNum, minNum)
		wordx = words[theNum[0]]
	}
	return wordx
}
