package users

import (
	"github.com/bot-o-mat-Prosp3r/blog"
	"github.com/bot-o-mat-Prosp3r/database"
	"github.com/bot-o-mat-Prosp3r/robots"
)

//User holds the struct data for user identity
type User struct {
	ID         int64  `json:"id"`
	Username   string `json:"username"`
	Email      string `json:"email"`
	Timejoined string `json:"timejoined,omitempty"`
	Status     string `json:"status"`
}

//Users holds user structs
var Users []User

//SetUser loads the user lists into the struct
func SetUser() {
	Users = nil
	//Pull from database
	result, err := database.DB.Query("SELECT * FROM users ORDER BY userid DESC")

	if err != nil {
		_ = blog.Botlog("Failed loading Users from Database: " + err.Error())
		//return nil
	}

	defer result.Close()

	for result.Next() {
		user := new(User)
		err = result.Scan(&user.ID, &user.Username, &user.Email, &user.Timejoined, &user.Status)
		if err != nil {
			//fmt.Println(err.Error())
			_ = blog.Botlog("Failed loading Users from Database: " + err.Error())
			//return nil
		}
		Users = append(Users, *user)
	}

	if err = result.Err(); err != nil {
		_ = blog.Botlog("Failed loading Robots from Database: " + err.Error())
		//return nil
	}
	//Reutrn the struct
	//return users
}

//GetUserID returns userid
func (m *User) GetUserID() int64 {

	if m == nil {
		return 0
	}
	return m.ID
}

//GetUserName return users' username
func (m *User) GetUserName() string {
	if m == nil {
		return ""
	}
	return m.Username
}

//GetUserEmail returns user email
func (m *User) GetUserEmail() string {
	if m == nil {
		return ""
	}
	return m.Email
}

//GetTimeJoined returns time joined
func (m *User) GetTimeJoined() string {
	if m == nil {
		return ""
	}
	return m.Timejoined
}

//GetUserStatus returns user status
func (m *User) GetUserStatus() string {
	if m == nil {
		return ""
	}
	return m.Status
}

//GetuserBots returns user's robot struct
func GetuserBots(email string) []robots.Robot {

	SetUser()

	var userid int64

	for _, user := range Users {
		if user.Email == email {
			userid = user.ID
		}
	}

	usersbots := GetMyBots(userid)
	return usersbots
}

//GetMyBots returns a list of given userid's bots
func GetMyBots(userid int64) []robots.Robot {
	var usersbots []robots.Robot

	allbots := robots.Robotx //GET ALL ROBOTS FROM ROBOT LIST IN STRUCT

	for _, botlist := range allbots {
		mybots := new(robots.Robot)
		if botlist.Userid == userid {
			mybots.ID = botlist.ID
			mybots.Name = botlist.Name
			mybots.Type = botlist.Type
			mybots.Userid = botlist.Userid
			mybots.Creationtime = botlist.Creationtime
		}
		usersbots = append(usersbots, *mybots)
	}
	return usersbots
}
