package blog

import (
	"log"
	"os"
)

//Botlog writes message to a log file in the log directory
func Botlog(message string) bool {

	logfile := "./boterror.log"
	f, err := os.OpenFile(logfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
		//fmt.Println(err.Error())
		//Pigeonlog(err.Error())
		return false
	}
	defer f.Close()

	log.SetOutput(f)
	log.Println(message)

	return true
}
