package main

import (
	"fmt"
	"time"

	"github.com/bot-o-mat-Prosp3r/database"
	"github.com/bot-o-mat-Prosp3r/robots"
)

//Start the botomath API server
func main() {
	//initiate database connection
	database.InitDB()
	//mock with fake names
	//_ = robots.makeBot("Julie", "UNIPEDAL")
	//SET ALL PRELOADABLE DATA =>SETBOTTYPES, SKILLTYPES, BOT-EXPERTISE
	_ = robots.SetSkillsFromTaskList()
	_ = robots.SetrobotTypes()  //loads robot types from json file
	robots.SetRobotTypeSkills() //Train robots

	for {

		fmt.Println(robots.Robottype)
		fmt.Println(robots.Robotskill)
		fmt.Println(" ")
		s := robots.Robotskilled{}
		fmt.Println(s.GetSkill("Arachnid"))
		//UPDATE WORK STRUCT
		//UPDATE USER STRUCT
		//UPDATE BOTS STRUCT
		//robots.SetRobots() //this refresh should be even dirven
		//users.GetuserBots(email)
		//userid := int64(1)
		//users.GetMyBots(userid)
		//UPDATE

		//=>_createUser(temp id is random hash or email) => //_savePersistUser => _confirmUser(email)
		//=>_CreateBot(s) => _AssignBotTask(s)
		//
		//<=_doTask <= //_fetchBotsTaskStatus
		//<=BotexcitementLevels & Message switch this every now and then
		//=>_boost_bots
		//<=See Other live players
		//=>InvitePlayer
		//<=Metrics
		//=>Get old bots using email
		time.Sleep(time.Millisecond * 2000) //wait 3 seconds before running again
	}
}

//SaveBot saves the newly created bot to database
func SaveBot(botname, bottype string) int64 {
	return 0
}

//GetAllBots fetchaes all bots from the Database
func GetAllBots() {}

//SetBots sets the buffer for all bots on the bots Structs
func SetBots() {}

//GetUserBots will return an array of a user's created bots
func GetUserBots() {}
