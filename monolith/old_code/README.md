# BOT-O-MAT
Use any language to complete this challenge. The implementation is up to you: it can be a command-line application or have a graphical interface.

Your application should collect a name and robot type from the types we list below. For each, it should create a Robot of the type the user chooses, e.g. Larry, Bipedal. 

Given the list of tasks below, your application should then assign the Robot a set of five tasks, all of which complete after a duration that we show in milliseconds. 



- Collect a name and robot type from user.
- Instantiate a Robot of the type provided by the user with the name provided by the user
  - for example: Bipedal, Larry
- Set up methods on Robot to complete tasks from the provided list

## Robot
Robot completes tasks and removes them from the list when they are done (i.e. enough time has passed since starting the task).

## Tasks
Tasks have a description and an estimated time to complete.

```
[
  {
    description: 'do the dishes',
    eta: 1000,
  },{
    description: 'sweep the house',
    eta: 3000,
  },{
    description: 'do the laundry',
    eta: 10000,
  },{
    description: 'take out the recycling',
    eta: 4000,
  },{
    description: 'make a sammich',
    eta: 7000,
  },{
    description: 'mow the lawn',
    eta: 20000,
  },{
    description: 'rake the leaves',
    eta: 18000,
  },{
    description: 'give the dog a bath',
    eta: 14500,
  },{
    description: 'bake some cookies',
    eta: 8000,
  },{
    description: 'wash the car',
    eta: 20000,
  },
]
```

## Types
```
{ 
  UNIPEDAL: 'Unipedal',
  BIPEDAL: 'Bipedal',
  QUADRUPEDAL: 'Quadrupedal',
  ARACHNID: 'Arachnid',
  RADIAL: 'Radial',
  AERONAUTICAL: 'Aeronautical'
}
```

## Features to add once the core functionality is complete
Be creative and have fun! Use this list or create your own features.
- Allow users to create multiple robots at one time
- Create a leaderboard for tasks completed by each Robot
- Create tasks specific for each robot type, this could work in conjunction with the leaderboard. For e.g. robots that are assigned tasks that their type can’t perform won’t get “credit” for finishing the task.
- Add persistance for tasks, bots and leaderboard stats


## Authors
- Scott Hoffman <https://github.com/scottshane>
- Olivia Osby <https://github.com/oosby>


##How to deploy
###(PROSP3R's NOTES)
- Install LAMP on Ubuntu (see instructions on how to below)
- Copy web files to (www/html/) folder. from local system run: scp -r ./* root@server_ip_address:/var/www/html/
- Copy the script(botomat.conf) to (/etc/init/) from server system run: cp /var/www/html/botomat.conf /etc/init/
- Make sure the (botdaemon.php) is copied to (/usr/share/) folder as well.
- Install upstart on server system run: sudo apt-get install upstart
- Robot helper(daemon) logs Logs are stored in /var/log/botomathUpstart_JobMaster.log

##RESTORE MYSQL
- mysqldump -u [username] -p [databaseName] > [filename]-$(date +%F).sql

mysqldump -u root -p botomath > 


##Configure Daemon(special only for Ubuntu 15.x and above)
##basically we're switching back to upstart.

Switching to upstart for Daemon functionality

- sudo apt-get install upstart-sysv
- sudo update-initramfs -u
- sudo apt-get purge systemd

#Reboot System

#Start Daemon

- sudo service botomat start

###Please note:
- Bots are trained on the fly to have random(types and number) of abilities as they're created( this means each bot is unique in their abilities )
- Bots don't get scores for tasks outside their abilities

##FIXED BUGS CAUSING MEMORY LEAKS.
- This latest update fixes the bugs causing memory leaks through JQuery and optimizes the DB calls by reducing them per request for leaderboard to total of one.
- Fixed freezing create button bug.

##WORKING VERSION
- http://149.56.133.249/
