<?php

/*
 * A simple class to handle most of the features and operations of BotOMath
 */
if(!class_exists('conf.php')){ include_once('conf.php'); }


class Bothomath{
    
    /**
     * Task limit will set the number of task a bot can be assigned on creation.
     */
    var $tasklimit = 5;
    
    /** file holding tasks their eta and description information*/
    var $taskData = '../data/task.json';
    
    /**File holding json data of bot types */
    var $botData = '../data/bots.json';
    
    var $botTypesJson;
    
    /**User information table */
    var $userTable = 'users';
    
    /**bots information details table*/
    var $botTable = 'bots';
    
    /** Task types table*/
    var $taskTable = 'tasktypes';
    
    /** Bot skills table one to many relationship */
    var $botSkillsTable = 'botskillset';
    
    /** Holds information of bot tasks */
    var $botTaskTable = 'bottasks';
    
    var $work_status = '';
    
    
    /**Add Default actions to be performed when called*/
    function __construct() {
        //preload bot types
        //will think of more things to trigger as the need arise
    }
    
    /**
     * loadBotTypesJson will load json file of bot types
     * @return array returns an associative array of bot types
     */
    function loadBotTypesJson(){
        $jsonString = file_get_contents($this->taskData);
        $botTypeJson = json_decode($jsonString, true); // decode the JSON into an associative array
        return $botTypeJson;
    }
    
    
    
    /**
     * createUser adds a new user to the user list
     * @param type string username
     * @return type int userid
     */
    function creatUser(){
        $status = 'a';
        //create a randome user name
        $username = $this->localHash(6, 'ALPHANUMERIC');
        $email = "temp-$username";
        $dbcon = pdo_dbcon();
        //INSERT USER INFORMATION
        $stmt = $dbcon->prepare(" INSERT INTO $this->userTable(userid, username, email, timejoined, status) VALUES(:userid, :username, :email, :timejoined, :status) ");
        $stmt->execute(array(':userid'=>NULL, ':username'=>$username, ':email'=>$email, ':timejoined'=>NULL, ':status'=>$status));
        $userid = $dbcon->lastInsertId();
        return $userid;
    }
    
    /**
     * returnUserId returns userid of given username
     * @param int userid
     * @param string specificUserInfo incase where user want just a specific information like username returned
     * @return array or string returns an associative array of userInfo or a string of the specific user information if supplied
     */
    function returnUserInfo( $userid, $specificUserInfo = null ){
        $dbcon = pdo_dbcon();
        $stmt = $dbcon->prepare(" SELECT userid, username, email, timejoined, status FROM $this->userTable WHERE userid =:userid ");
        $stmt->execute( array(':userid'=>$userid) );
        $userInfo = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if( isset($specificUserInfo) && strlen($specificUserInfo) > 5 ){
            return $userInfo[$specificUserInfo];
        }else{
            return $userInfo;
        }
    }        
    
    /**
     * updateUserInfo Updates user information of given userid
     * @param int $userid
     * @param string $username
     * @param string $email
     * @param string $status Optional
     * @return int number affected user in the update is returned above 0 is a success
     */
    function updateUserInfo( $userid, $username, $email, $status = null ){
        $dbcon = pdo_dbcon();
        $stmt = $dbcon->prepare(" UPDATE $this->userTable SET username = :username, email = :email, status = :status WHERE userid=:userid ");
        $stmt->execute(array(':userid' => $userid, ':username'=>$username, ':email'=>$email, ':status'=>$status ));
        $rows = $stmt->rowCount();
        return $rows; 
    }
    
    /**
     * createBot Creates a new bot using the user given bot name and the chosen bot type
     * @param int $userid
     * @param string botName
     * @param string botType
     * @return int unique-botid
     */
    //function createBot( $userName, $userid, $botType ){
    function createUserBot( $userid, $botName, $botType ){
        //ToDO Create BOT
        $length = 4;
        $status = 'a';//active
        $hashtype = "ALPHANUMERIC";
        //$userName = $this->returnUserInfo($userid, 'username');
        $dbcon = pdo_dbcon();
        //INSERT INFO IN DATABASE
        $stmt = $dbcon->prepare(" INSERT INTO $this->botTable(botid, botname, bottype, userid, timecreated, status) VALUES(:botid, :botname, :bottype, :userid, :timecreated, :status) ");
        $stmt->execute( array(':botid'=>NULL, ':botname'=>$botName, ':bottype'=>$botType, ':userid'=>$userid, ':timecreated'=>NULL, ':status'=>$status) );
        $botid = $dbcon->lastInsertId();
        
        if($botid > 0){
            //insert success.
            //train bot
            //Get all available skills
            $taskTypes = $this->fetchAllTask();
            /*skill set in skill array teach bot random skills*/
            $skillNum = rand(2, 4);//teach a random number of skills to each bot
            $counter = 1;
            
            foreach( $taskTypes as $skill ){
                $skillid = $skill['taskid'];
                if($counter <= $skillNum ){
                    $this->teachBotSkills( $botid, $skillid );
                }
                $counter++;
            }
            $this->createBotTask($botid, $taskTypes);
            return $botid;
        }else{
            //failed
            return null;
        }
    }
   
    /**
     * fetchBots returns a list of all bots created by given user id or all bots in database if userid is not supplied.
     * @param int $userid
     * @return array associative array of returned Bots information
     */
    function fetchBots( $userid = null ){
        //return all if name is not set, else return user's bots only
        $dbcon = pdo_dbcon();
        if($userid != null){
            $stmt = $dbcon->prepare(" SELECT botid, botname, bottype, userid, timecreated, status FROM $this->botTable WHERE userid = :userid ORDER BY timecreated "); 
            $stmt->execute(array(':userid'=>$userid));
        }else{
            $stmt = $dbcon->prepare(" SELECT botid, botname, bottype, userid, timecreated, status FROM $this->botTable ORDER BY timecreated ");
            $stmt->execute();
        }
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }
    
    /**
     * fetchLeadBoardData returns an associative array of bots and their task information in a single query. this is supposed to reduce the DB call amounts.
     * @param $userid optional
     * @return array $leadBoard associative array of bot and task data
     */
    function fetchLeadBoardData($userid = ""){
        //SELECT bots.`botid`, bots.`botname`, bots.`bottype`, bots.`userid`, bots.`timecreated`, bots.`status`, bottasks.`jobid`, bottasks.`botid`, bottasks.`taskid`, bottasks.`taskdescription`, bottasks.`tasketa`, bottasks.`taskscore`, bottasks.`starttime`, bottasks.`endtime`, bottasks.`taskstatus` FROM bots, bottasks WHERE bots.`userid`= '12' AND bottasks.`botid` = bots.`botid`;
        $dbcon = pdo_dbcon();
        $boti = $this->botTable;
        $taski = $this->botTaskTable;
        if( isset($userid) && ($userid > 0)){
            $stmt = $dbcon->prepare(" SELECT $boti.`botid`, $boti.`botname`, $boti.`bottype`, $boti.`userid`, $boti.`timecreated`, $boti.`status`, $taski.`jobid`, $taski.`botid`, $taski.`taskid`, $taski.`taskdescription`, $taski.`tasketa`, $taski.`taskscore`, $taski.`starttime`, $taski.`endtime`, $taski.`taskstatus` FROM $boti, $taski WHERE $boti.`userid`=:userid AND $taski.`botid` = $boti.`botid` ");
            $stmt->execute(array(':userid'=>$userid));
            $leadBoard = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }else{
            $stmt = $dbcon->prepare(" SELECT $boti.`botid`, $boti.`botname`, $boti.`bottype`, $boti.`userid`, $boti.`timecreated`, $boti.`status`, $taski.`jobid`, $taski.`botid`, $taski.`taskid`, $taski.`taskdescription`, $taski.`tasketa`, $taski.`taskscore`, $taski.`starttime`, $taski.`endtime`, $taski.`taskstatus` FROM $boti, $taski WHERE $taski.`botid` = $boti.`botid` ");
            $stmt->execute();
            $leadBoard = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }   
        return $leadBoard;
    }
    
    
    /**
     * getBotTaskInfo will return a single all round results of bots and their tasks with one query
     * @param int $userid
     * @return array $botInfo associative array of bot and tasks info for bots display
     */
    function getBotTaskInfo( $userid = ""){
        //SELECT bots.`botid`, bots.`botname`, bots.`bottype`, bots.`status`, bots.`timecreated`, bots.`userid`, bottasks.`jobid`, bottasks.`taskid`, bottasks.`taskdescription`, bottasks.`tasketa`, bottasks.`taskscore`, bottasks.`taskstatus`, bottasks.`starttime`, bottasks.`endtime` FROM bots, bottasks WHERE bots.`userid` = '12' AND bots.`botid` = bottasks.`botid`;
        $dbcon = pdo_dbcon();
        $boti = $this->botTable;
        $taski = $this->botTaskTable;
        if( isset($userid) && ($userid > 0)){
            $stmt = $dbcon->prepare(" SELECT $boti.`botid`, $boti.`botname`, $boti.`bottype`, $boti.`status`, $boti.`timecreated`, $boti.`userid`, $taski.`jobid`, $taski.`taskid`, $taski.`taskdescription`, $taski.`tasketa`, $taski.`taskscore`, $taski.`taskstatus`, $taski.`starttime`, $taski.`endtime` FROM $boti, $taski WHERE $boti.`userid` =:userid AND $boti.`botid` = $taski.`botid` ");
            $stmt->execute(array(':userid'=>$userid));
            $botInfo = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }else{
            $stmt = $dbcon->prepare(" SELECT $boti.`botid`, $boti.`botname`, $boti.`bottype`, $boti.`status`, $boti.`timecreated`, $boti.`userid`, $taski.`jobid`, $taski.`taskid`, $taski.`taskdescription`, $taski.`tasketa`, $taski.`taskscore`, $taski.`taskstatus`, $taski.`starttime`, $taski.`endtime` FROM $boti, $taski WHERE $boti.`botid` = $taski.`botid` ");
            $stmt->execute();
            $botInfo = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }   
        return $botInfo;
    }

    
    /**
     * returnBotInfo returns specific bot information value if specified else returns an array of all bot information
     * @param int $botid
     * @param string $specificBotInfo
     * @return string or array depending on 
     */
    function returnBotInfo( $botid, $specificBotInfo = null ){
        $dbcon = pdo_dbcon();
        $stmt = $dbcon->prepare(" SELECT botid, botname, bottype, userid, timecreated, status FROM $this->botTable WHERE botid =:botid ");
        $stmt->execute( array(':botid'=>$botid) );
        $botInfo = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if( isset($specificBotInfo) && strlen($specificBotInfo) > 5 ){
            return $botInfo[0][$specificBotInfo];//string
        }else{
            return $botInfo;//associative array
        }
    }
    
     /**
     * teachBotSkills will add skills to a bots brain i.e. the botsSkillTable
     * @param int $botid
     * @param int $taskid
     * @return int $skillId
     */
    function teachBotSkills( $botid, $taskid ){
        //INSERT INFO IN DATABASE
        $dbcon = pdo_dbcon();
        $stmt = $dbcon->prepare(" INSERT INTO $this->botSkillsTable(entryid, botid, taskid) VALUES(:entryid, :botid, :taskid) ");
        $stmt->execute( array(':entryid'=>NULL, ':botid'=>$botid, ':taskid'=>$taskid) );
        $skillid = $dbcon->lastInsertId();
        return $skillid;
    }
    
    
    /**
     * returnBotSkills will return the kinds of task a bot is skilled at i.e. equipped to carry in order to help us assign scores for work done
     * @param int $botid
     * @return array an associative array of bot skills and botid
     */
    function returnBotSkills( $botid ){
        $dbcon = pdo_dbcon();
        $stmt = $dbcon->prepare(" SELECT entryid, botid, taskid FROM $this->botSkillsTable WHERE botid=:botid ");
        $stmt->execute( array(':botid'=>$botid) );
        $botSkillArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $botSkillArray;
    }
    
    
    function displayBotSkills( $botid ){
        $botSkills = $this->returnBotSkills($botid);
        $skillRow = "";
        foreach($botSkills as $skill ){
            $taskid = $skill['taskid'];
            #$skillName = $this->returnTaskInfo( $taskid, 'taskdescription' );
            $skillName = $this->returnTaskInfo( $taskid );
            //var_dump( $skillName );
            $skillRow.= "<br />- ".$skillName[0]['taskdescription'] ; 
        }
        return $skillRow;
    }
    /**
     * fetchAllTask will return all the types of task and their information in an associative array
     * @return array $taskList associative array of task information
     */
    function fetchAllTask(){
        $dbcon = pdo_dbcon();
        $taskstatus = 'a'; //active flag
        $stmt = $dbcon->prepare(" SELECT taskid, taskdescription, tasketa, taskscore, taskstatus FROM $this->taskTable WHERE taskstatus = :taskstatus ");
        $stmt->execute( array(':taskstatus'=>$taskstatus) );
        $taskInfo = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $taskInfo;//associative array
        }
    
    
    /**
     * returnTaskInfo Returns the details of a particular task
     * @param int $taskid
     * @param string $specificTastInfo (optional) specifies which column information to return instead of the entire associative array
     * @return array $taskInfo or string 
     */
    function returnTaskInfo( $taskid, $specificTaskInfo = null ){
        $dbcon = pdo_dbcon();
        $stmt = $dbcon->prepare(" SELECT taskid, taskdescription, tasketa, taskscore, taskstatus FROM $this->taskTable WHERE taskid =:taskid ");
        $stmt->execute( array(':taskid'=>$taskid) );
        $taskInfo = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if( isset($specificTaskInfo) && strlen($specificTaskInfo) > 5 ){
            return $taskInfo[$specificTaskInfo];//string
        }else{
            return $taskInfo;//associative array
        }
    }
    
    /**
     * createBotJob creates a new job for the robot
     * @param int $botid
     * @param string $taskdescription
     * @param int $tasketa
     * @param int $taskscore
     * @return int $taskid
     */
    function createBotJob( $botid, $taskid, $taskdescription, $tasketa, $taskscore ){
        $dbcon = pdo_dbcon();
        //INSERT INFO IN DATABASE
        $taskstatus = 0;
        //
        $microNow = DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
        $starttime = $microNow->format("Y-m-d H:i:s.u");
        
        //$endtime = "0000-00-00 00:00:00.000000";
        $etaInMicroSecs = $this->milliToMicroSeconds($tasketa);
        $endtime = $this->addMicroSecondsToTime($starttime, $etaInMicroSecs);
        
        //CONVERT('2007-11-30 10:30:19',datetime(6))
        $stmt = $dbcon->prepare(" INSERT INTO $this->botTaskTable(jobid, taskid, botid, taskdescription, tasketa, taskscore, starttime, endtime, taskstatus) VALUES(:jobid, :taskid, :botid, :taskdescription, :tasketa, :taskscore, :starttime, :endtime, :taskstatus) ");
        $stmt->execute( array(':jobid'=>NULL, ':taskid'=>$taskid, ':botid'=>$botid, ':taskdescription'=>$taskdescription, ':tasketa'=>$tasketa, ':taskscore'=>$taskscore, ':starttime'=>$starttime, ':endtime'=>$endtime, ':taskstatus'=>$taskstatus) );
        $jobid = $dbcon->lastInsertId();
        return $jobid;
    }
    
    /**
     * fetchBotJobs returns a list of jobs currently on the assigned task table by the bot of given $botid
     * @param int $botid
     * @return array $botJobsArray
     */
    function fetchBotJobs( $botid ){
        //var_dump($botid);
        //exit;
        $dbcon = pdo_dbcon();
        $stmt = $dbcon->prepare(" SELECT jobid, taskid, botid, taskdescription, tasketa, taskscore, starttime, endtime, taskstatus FROM $this->botTaskTable WHERE botid = :botid ");
        $stmt->execute( array(':botid'=>$botid) );
        $botJobsArray = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $botJobsArray;
    }
    
    /**
     * 
     */
    function getPercentDone($botid, &$percentDone){
        $totalScore = 0;
        $work_doneCount = 0;
        $work_leftCount = 0;
        $jobs = $this->fetchBotJobs( $botid );
        foreach( $jobs as $key => $job ){
            $jobid = $job['jobid'];
            $jobdescription = $job['taskdescription'];
            $taskscore = $job['taskscore'];
            $jobStatus = $job['taskstatus'];
            
            //$work_status = 'Working';
                    if($jobStatus == 1){ 
                        //ie. done
                        $totalScore+= $taskscore; 
                        $work_doneCount++;
                    }else{
                        $work_leftCount++;
                    }
                        //Percentage work done and status
                        $percent_done = ( ($work_doneCount*100) / ($work_doneCount + $work_leftCount));
                        /*if($percent_done < 100){
                            $work_status = 'Working..';
                        }else{
                            $work_status = 'Done';
                        }*/
                        $percentDone = $percent_done;
        }
        return $percentDone;
    }
    
    /**
     * getBotJobsDisplay returns a string of jobs by the robot of given id and the points scored
     */
    function getBotJobsDisplay($botid ){
        $botJobs = $this->fetchBotJobs( $botid );
            
        
            $work_leftCount = 0;
            $work_doneCount = 0;
            $totalScore = 0;
            $prospectTotalScore = 0;
            $task_list = "";
        
            foreach($botJobs as $job ){
                    
                    $jobid = $job['jobid'];
                    $jobdescription = $job['taskdescription'];
                    $taskscore = $job['taskscore'];
                    $jobStatus = $job['taskstatus'];
                    $work_status = 'Working';
                    if($jobStatus == 1){ 
                        //ie. done
                        $totalScore+= $taskscore; 
                        $work_doneCount++;
                    }else{
                        $work_leftCount++;
                    }
                    $task_list.="<br>  $jobdescription ($taskscore points) <br/>";
                    $prospectTotalScore+= $taskscore;
                    if($work_doneCount < $work_leftCount){
                        $work_status = 'Working';
                    }else{
                        $work_status = 'Done';
                    }
                }
                return $task_list;
    }
    
    /**
     * returnBotJobInfo returns associative array of information about a particular task assigned to given botid. It'll return string if specific information about that task option is specified
     * @param int $taskid
     * @param string $specificTaskInfo
     * @return string or array of task information
     */
    function returnBotJobInfo( $taskid, $specificTaskInfo = null ){
        $dbcon = pdo_dbcon();
        $stmt = $dbcon->prepare(" SELECT taskid, botid, taskdescription, tasketa, taskscore, starttime, endtime, taskstatus FROM $this->taskTable WHERE taskid =:taskid ");
        $stmt->execute( array(':taskid'=>$taskid) );
        $jobInfo = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if( isset($specificTaskInfo) && strlen($specificTaskInfo) > 5 ){
            return $jobInfo[$specificTaskInfo];//string
        }else{
            return $jobInfo;//associative array
        }
    }
    
    /**
     * botHasSkill will return true if bot has been trained in a particular skill or false if not
     * @param int $skillid
     * @param int $botid 
     * return boolean true if the bot has been trained in that skill of given taskId
     */
    function botHasSkill( $skillid, $botid ){
        $botSkillset = $this->returnBotSkills( $botid );
        foreach( $botSkillset as $skill ){
            if( $skill['taskid'] == $skillid ){
                return true;
            }
        }
        return false;
    }
    
    /**
     * createBotTask will manage the process of creating tasks for a bot
     * @param int $botid
     * @return int $taskid
     */
    function createBotTask(int $botid, $taskTypes){
        //Get all tasks
            //$taskTypes = $this->fetchAllTask();
            /*skill set in skill array teach bot random skills*/
            $skillNum = rand(2, 4);//teach a random number of skills to each bot
            $counter = 1;
            
            for($x=0; $x < $this->tasklimit; $x++){
                $totalAmountTasks = count($taskTypes);
                //var_dump($taskTypes);
                //count down the number of the random upper range within the range of number of task available
                $taskNum = rand(0, $totalAmountTasks); //rand(0, $totalAmountTasks-$counter);
                //var_dump("TOTAL:".$totalAmountTasks);
                //var_dump($taskNum);
                //exit;
                $newTaskid = $taskTypes[$taskNum]['taskid'];
                $taskdescription = $taskTypes[$taskNum]['taskdescription'];
                $tasketa = $taskTypes[$taskNum]['tasketa'];
                $taskscore = $taskTypes[$taskNum]['taskscore'];//only award the score if it's part of the bot's abilities
                
                /*$newTaskid = $taskTypes[$taskNum]['taskid'];
                $taskdescription = $taskTypes[$taskNum]['taskdescription'];
                $tasketa = $taskTypes[$taskNum]['tasketa'];
                $taskscore = $taskTypes[$taskNum]['taskscore'];*///only award the score if it's part of the bot's abilities
                if($this->botHasSkill($newTaskid, $botid) == false){
                    //bot is not skilled in this task, award full score
                    $taskscore = 0;
                }
                
                //hack alert! to save some time...reverse the count of $x if there's a read error on task information
                if($newTaskid != NULL){
                    //add new task
                    $this->createBotJob($botid, $newTaskid, $taskdescription, $tasketa, $taskscore);
                    //remove that task from whole task array
                    unset($taskTypes[$taskNum]);//remove already assigned element
                    $taskTypes = array_values($taskTypes);//normalize array indexes
                    $counter++;
                }else{
                    $x--;
                }
            }
            return $counter;
    }
    
    /**
     * Return Json array of leadbord data. botid, name, taskCompleted, scores
     * @param int $userid 
     * @return json leadboard data
     */
    function readScoreBoard( $userid ){
        $fetchuserBots = $this->fetchBots( $userid );
        $leadBoard_row = array();
        foreach($fetchuserBots as $bots){
            //botid, botname, bottype, userid, timecreated, status
            $botid = $bots['botid'];
            $botname = $bots['botname'];
            $bottype = $bots['bottype'];
            
            $botjobs = $this->fetchBotJobs( $botid );
            foreach( $botjobs as $job ){
                $jobid = $job['jobid'];
                $taskid = $job['taskid'];
                $taskdescription = $job['taskdescription'];
                $tasketa = $job['tasketa'];
                $taskscore = $job['taskscore'];
                $taskstatus = $job['taskstatus'];
                if($taskstatus == 1){
                    //job is done
                    $botjobinfo = ["botid"=>$botid, "botname"=>$botname, "taskdescription"=>$taskdescription, "taskscore"=>$taskscore];
                    array_push($leadBoard_row, $botjobinfo);
                }
            }
        }
        return $leadBoard_row;
    }
    
    
    
    /**
     * localHash will return a random generated hash 
     * @param int $length how many characters you want the generated has to be.
     * @param string $hashtypes type of characters you want in the hash. The three options are: ALPHABETIC, ALPHANUMERIC, NUMERIC, ALLTHREE
     * @return string $hash Generated hash
     */
    function localHash($length, $hashtype){
            $alpha = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
            $num = array('1','2','3','4','5','6','7','8','9','0');
            $spe = array('!','@','#','$','%','^','&','*','(',')','_','+','~','.','/',';','\\',']','[','{','}','|',':',';','>','<','?','-','=');
            
            $result = "";
            if( $length < 4){
                return 0;
            }else{
                #THEN CARRY OUT THE OPERATION
                if( $hashtype == 'ALPHABETIC' ){
                    #ALPHABETIC HASH
                    for( $x = 0; $x < $length; $x++ ){
                            $result.=$alpha[rand(0,count($alpha))]; 
                    }
                }else if( $hashtype == 'ALPHANUMERIC'){
                    #THEN CARRY OUT THE OPERATION FOR GENERAL ALL ENCOMPASSING SHASH
                    for( $x = 0; $x < $length; $x++ ){
                                            $randy = rand(1,2);
                        if( $randy == 1 ){
                            $result.=$alpha[rand(0,count($alpha))];
                        }
                        if( $randy == 2 ){
                            $result.=$num[rand(0,count($num))];
                        }
                    }
                }else if( $hashtype == 'NUMERIC' ){
                    #NUMERIC HASH
                    for( $x = 0; $x < $length; $x++ ){
                            $result.=$num[rand(0,count($num))];
                    }
                }else{
                    #GENARAL HASH
                    #THEN CARRY OUT THE OPERATION FOR GENERAL ALL ENCOMPASSING SHASH
                    for( $x = 0; $x < $length; $x++ ){
                                            $randyx = rand(1,3);
                        if( $randyx == 1 ){
                            $result.=$alpha[rand(0,count($alpha))];
                        }
                        if( $randyx == 2 ){
                            $result.=$num[rand(0,count($num))];
                        }
                        if( $randyx == 3 ){
                            $result.=$num[rand(0,count($spe))];
                        }
                    }
                }
                
            }//END IF
            return $result;
        }
        
        
        /**
         * microTimeDifference returns a positive difference between time times
         * @param time $timeOne 
         * @param time $timeTwo
         * @return int microtime value from the difference
         */
        function microTimeDifference( $timeOne, $timeTwo){
            $microTimeOne = returnMicroSeconds( $timeOne );
            $microTimeTwo = returnMicroSeconds( $timeTwo );
            $difference = abs( $microTimeOne-$microTimeTwo );
            return $difference;
        }
        
        /**
         * returnMicroSeconds takes a complete datetime(6) value and returns the microsecond portion of it.
         * @param datetime(6)
         * @return int $microsec
         */
        function returnMicroSeconds( $timeValue ){
            $timex = explode('.', $timeValue);
            $microSec = $timex[1];
            return $microSec;
        }
        
        /**
         * returnDateTime
         */
        function returnDateTime( $timeValue ){
            $timex = explode('.', $timeValue);
            $mainTime = $timex[0];
            return $mainTime;
        }
        
        /**
         * addMicroSecondsToTime 
         */
        function addMicroSecondsToTime( $timeValue, $microSec ){
            $fmicrosec = $this->returnMicroSeconds( $timeValue );
            $totalMS = $fmicrosec + $microSec;
            $mainTime = $this->returnDateTime( $timeValue );
            return "$mainTime.$totalMS";
        }
        
        /**
         * milliToMicroSeconds will convert a millisecond value to microsecond
         * @param int milliseconds
         * @return int microseconds converted value
         */
        function milliToMicroSeconds( $milliSecs ){
            return round($milliSecs / 1000);
        }
        
        /**
         * microToMilliSeconds will convert a microsecond value to millisecond
         * @param int microSecs
         * @return int milliseconds converted value
         */
        function microToMilliSeconds( $microSecs ){
            return round($microSecs * 1000);
        }
}