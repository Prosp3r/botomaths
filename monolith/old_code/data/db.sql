#PREPARING MATHOBOT TABLES

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`(
`userid` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
`username` VARCHAR(50),
`email` VARCHAR(50) UNIQUE,
`timejoined` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`status` VARCHAR(1)
);

DROP TABLE IF EXISTS `bots`;
CREATE TABLE `bots`(
`botid` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
`botname` VARCHAR(100),
`bottype` VARCHAR(20),
`userid` INT(11),
`timecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`status` VARCHAR(1)
);

DROP TABLE IF EXISTS `tasktypes`;
CREATE TABLE `tasktypes` (
 `taskid` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
 `taskdescription` int(11),
 `tasketa` decimal(14,2),
 `taskscore` decimal(14,2),
 `taskstatus` VARCHAR(1)
 );

CREATE TABLE `botskillset`(
 `entryid` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
 `botid` int(11),
 `taskid` int(11)
);

DROP TABLE IF EXISTS `bottasks`;
CREATE TABLE `bottasks`(
`taskid` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
`botid` int(11),
`taskdescription` text,
`tasketa` int(11),
`taskscore` int(11),
`starttime` datetime,
`endtime` datetime,
`taskstatus` int(11)
);




CREATE TABLE `bottasks` (
  `jobid` int(11) NOT NULL AUTO_INCREMENT,
  `taskid` int(11) DEFAULT NULL,
  `botid` int(11) DEFAULT NULL,
  `taskdescription` text DEFAULT NULL,
  `tasketa` int(11) DEFAULT NULL,
  `taskscore` int(11) DEFAULT NULL,
  `starttime` datetime(6) NOT NULL DEFAULT '0000-00-00 00:00:00.000000' ON UPDATE current_timestamp(),
  `endtime` datetime(6) DEFAULT NULL,
  `taskstatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`jobid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;