<?php
session_start();
//session_destroy();
//Include the main botomath class
if(!class_exists('classes/botomathClass.php')){ include_once('classes/botomathClass.php'); }
$bom = new Bothomath;

//Get Bot and name information
if(isset($_POST['botname']) && isset($_POST['bottype'])){
    $botNamex = $_POST['botname'];
    $botTypex = $_POST['bottype'];
    #$sendTime = $_POST['sendtime'];
    
//Set userid
    if(!isset($_SESSION['userid'])){
        $userid = $bom->creatUser();
        $_SESSION['userid'] = $userid;
    }else{
        $userid = $_SESSION['userid'];
    }
//END USERID SETTING
    $botName_arr = json_decode($botNamex);
    $botType_arr = json_decode($botTypex);
    $arr_length = count($botName_arr);
    for( $x = 0; $x < $arr_length; $x++ ){
        $botName = $botName_arr[$x];
        $botType = $botType_arr[$x];
        if(strlen($botName) > 1){
            $botid = $bom->createUserBot($userid, $botName, $botType);
        }else{
            $message = "Bot names have to be more than one character to be created";
        }
    }
    //$botid = $bom->createUserBot($userid, $botName, $botType);
    //if(){
    //}
    $message = " $arr_length Bots have been created";
    echo $message;
}


//LOAD BOT TYPES

//LOAD TASK TYPES?

//LOAD BOT JOBS
if(isset($_POST['fetchbots'])){
    if(isset($_SESSION['userid'])){
        $userbots = $bom->fetchBots( $_SESSION['userid'] );
        
       
        $botsRow = '';

        foreach( $userbots as $bots ){
            $botid = $bots['botid'];
            $botname = $bots['botname'];
            $botType = $bots['bottype'];
            $bottypelow = strtolower($botType);
            
            $totalScore = 0;
            $work_leftCount = 0;
            $work_doneCount = 0;
            $task_list = '';
            $prospectTotalScore = 0;
            
            $botJobs = $bom->fetchBotJobs( $botid );
            
            
            
                
                $x = 0;
                while( $x < 5 ){
                    
                    $jobid = $botJobs[$x]['jobid'];
                    $jobdescription = $botJobs[$x]['taskdescription'];
                    $taskscore = $botJobs[$x]['taskscore'];
                    $jobStatus = $botJobs[$x]['taskstatus'];
                    
                    
                    if($jobStatus == 1){ 
                        //ie. done
                        $totalScore+= $taskscore; 
                        $work_doneCount++;
                    }else{
                        $work_leftCount++;
                    }
                    $task_list.="<br> $jobdescription ($taskscore points)";
                    $prospectTotalScore+= $taskscore;
                    $x++;
                }
                
                
                    
                $botSkills = $bom->displayBotSkills($botid);
                
            //$work_status = 
            $botsRow.="
             <div class='botgal $bottypelow' id='$botid'> 
        Robot Number:<span stlye='display:none;' id='botiddisplay'>$botid<br></span>
        Name: <span id='botname'>$botType, $botname</span><br /><br /><strong>Task list(points per task):</strong><br />
        
        <span id='tasklist'>$task_list</span><br><br>
         
        <span id='botworkstatus'></span><br><a name=''><span id='percentdone'></span>%</a>&nbsp;done
            
        <br />Total score: <span id='bottotalscore'></span>(of <a href='#' id='prospecttotalscore'>$prospectTotalScore</a>)<br />        
        <a onclick=\"document.getElementById('botprofile_$botid').style.display=''; \" class='pointer' role='button'>See my skill profile</a>
        <div id='botprofile_$botid' style='display:none;'>
           <strong> Though I can do all things.<br />I'm trained to:</strong>$botSkills
           <br />
           <a onclick=\"document.getElementById('botprofile_$botid').style.display='none'; \"  class='pointer' role='button'>Hide skills</a>
        </div>
        
        </div>";
        }
        echo $botsRow;
    }else{
        //"No session started";
    }
}










//POPULATE BOT INFORMATION INDIVIDUALLY    
    if(isset($_POST['fetchbotdetails'])){
        $botid = $_POST['fetchbotdetails'];
        $metric = $_POST['metric'];
        
        $botJobs = $bom->fetchBotJobs( $botid );
        #$botJobs = $bom->fetchBotJobs( $botid );
        $percentDone = 0; 
        
        
                $work_leftCount = 0;
                $work_doneCount = 0;
                $totalScore = 0;
                $prospectTotalScore = 0;
                $task_list = "";
                $work_status = '';
                
                foreach($botJobs as $job ){
                    
                    $jobid = $job['jobid'];
                    $jobdescription = $job['taskdescription'];
                    $taskscore = $job['taskscore'];
                    $jobStatus = $job['taskstatus'];
                    
                    //$work_status = 'Working';
                    if($jobStatus == 1){ 
                        //ie. done
                        $totalScore+= $taskscore; 
                        $work_doneCount++;
                    }else{
                        $work_leftCount++;
                    }
                        
                    $task_list.=" $jobdescription ($taskscore points)";
                    $prospectTotalScore+= $taskscore;
                    
                }
                
                $botUpdate = NULL;
                //
                //Percentage work done and status
                // $percent_done = ( ($work_doneCount*100) / ($work_doneCount + $work_leftCount));
                $percent_done = $bom->getPercentDone($botid, $percentDone);
                if($percent_done < 100){
                    $work_status = 'Working..';
                }else{
                    $work_status = 'Done';
                }
                $botInfoArray = array("totalscore"=>$totalScore, "percentdone"=>$percentDone, "workstatus"=>$work_status);
        
        //if($metric == 'botworkstatus'){ $botUpdate = $work_status; }
        if($metric == 'botworkstatus'){ $botUpdate = json_encode($botInfoArray); }
        //if($metric == 'botworkstatus'){ $botUpdate = json_encode($work_ray); }
        if($metric == 'percentage'){ $botUpdate = $percent_done; }
        //if($metric == 'workdone'){ $botUpdate = $botInfoArray; }
        
        echo $botUpdate;
    }
    
    
/**
 * Return leadboard
 */   
/*    
if(isset($_POST['fetchleadboard'])){
    if(isset($_SESSION['userid'])){
        $userid = $_SESSION['userid'];
        $leadborad = "";
        $totalBots = 0;
        $totalTask = 0;
        $totalTaskDone = 0;
        $totalScoresEarned = 0;
        //$leadboradx = $bom->readScoreBoard( $userid );
        $fetchuserBots = $bom->fetchBots( $userid );
        $leadBoard_row = array();
        foreach($fetchuserBots as $bots){
            //botid, botname, bottype, userid, timecreated, status
            $botid = $bots['botid'];
            $botname = $bots['botname'];
            $bottype = $bots['bottype'];
            $totalBots++;
            $botjobs = $bom->fetchBotJobs( $botid );
            for($x=0; $x<5; $x++){
                //var_dump();
                //exit;
                $taskdescription = $botjobs[$x]['taskdescription'];
                $botid = $botjobs[$x]['botid'];
                $taskStatus = $botjobs[$x]['taskstatus'];
                $taskdescription = $botjobs[$x]['taskdescription'];
                $taskscore = $botjobs[$x]['taskscore'];
                if($taskscore > 0){ 
                    $scoreClass = 'greentext'; 
                    $totalTaskDone++;
                    $totalScoresEarned+=$taskscore; 
                 }else{ 
                     $scoreClass = 'yellowtext'; 
                 }
                if($taskStatus == 1){
                    $leadborad.="<strong>Bot name: $bottype, $botname</strong> [$taskdescription][Credit earned:<a class='$scoreClass'>$taskscore</a>] &nbsp;&nbsp;&nbsp;";
                }
           }     
        }
        $totalTask = $totalBots*5;
        $leadborad.="&nbsp;&nbsp;&nbsp;SUMARY [ Total Bots: <a class='yellowtext'>$totalBots</a> &nbsp; Total Tasks: $totalTask [Task Done: <a class='greentext'>$totalTaskDone</a>] &nbsp; Total Scores Earned: <a class='greentext'>$totalScoresEarned</a> ]";
    }else{
        $leadborad = "You need to create a bot or two to see activity<br />";
    }
    echo $leadborad;
}*/
    
//fetchLeadBoardData($userid);
if(isset($_POST['fetchleadboard'])){
    if(isset($_SESSION['userid'])){
        $userid = $_SESSION['userid'];
        $leadborad = "";
        $totalBots = 0;
        $totalTask = 0;
        $totalTaskDone = 0;
        $totalScoresEarned = 0;
        //$leadboradx = $bom->readScoreBoard( $userid );
        $fetchuserBots = $bom->fetchLeadBoardData($userid);
        //$leadBoard_row = array();
        //var_dump($fetchuserBots);
        //exit;
        foreach( $fetchuserBots as $lead ){
            //var_dump($lead['botid']);
            //exit;
            $botid = $lead['botid'];
            $botname = $lead['botname'];
            $bottype = $lead['bottype'];
            $taskdescription = $lead['taskdescription'];
            $botid = $lead['botid'];
            $taskStatus = $lead['taskstatus'];
            $taskscore = $lead['taskscore'];
            if($taskscore > 0){
                    $scoreClass = 'greentext'; 
                    $totalScoresEarned+=$taskscore; 
            }else{
                $scoreClass = 'yellowtext'; 
            }
            
            if($taskStatus == 1){
                $totalTaskDone++;
                $leadborad.="<strong>Bot name: $bottype, $botname</strong> [$taskdescription][Credit earned:<a class='$scoreClass'>$taskscore</a>] &nbsp;&nbsp;&nbsp;";
            }
            $totalTask++;
        }
        $totalBots = $totalTask/5;
        $leadborad.="&nbsp;&nbsp;&nbsp;SUMARY [ Total Bots: <a class='yellowtext'>$totalBots</a> &nbsp; Total Tasks: $totalTask [Task Done: <a class='greentext'>$totalTaskDone</a>] &nbsp; Total Scores Earned: <a class='greentext'>$totalScoresEarned</a> ]";
        
    }else{
        $leadborad = "You need to create a bot or two to see activity<br />";
    }
        echo $leadborad;
}
