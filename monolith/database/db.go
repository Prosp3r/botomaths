package database

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/bot-o-mat-Prosp3r/blog"
	_ "github.com/go-sql-driver/mysql"
)

//Databaseinfo stores the access information for the database
type Databaseinfo struct {
	Host             string `json:"host"`
	Port             string `json:"port"`
	Databasename     string `json:"databasename"`
	Adminusername    string `json:"adminusername"`
	Databasepassword string `json:"databasepassword"`
}

var dbcred = Databaseinfo{}
var dataSourceName string

//DB global exported db connection
var DB *sql.DB

//SetDBInfo is a temporary measure. It reads the database info from the database json credential file
func SetDBInfo() error {

	dbfile := "botomath.json" //TEST ONLY I'LL USE ENV VARIABLE IN PRODUCTION
	fs, err := os.Open(dbfile)
	f, _ := ioutil.ReadAll(fs)

	if err != nil {
		fmt.Println(err.Error())
		_ = blog.Botlog("Error reading dbfile file : " + err.Error())
		return err
	}
	defer fs.Close()

	if err = json.Unmarshal(f, &dbcred); err != nil {
		//fmt.Println(err.Error())
		_ = blog.Botlog("Error Unmarshalling dbfile file : " + err.Error())
		return err
	}

	for i := 0; i < 1; i++ {
		dataSourceName = dbcred.Adminusername + ":" + dbcred.Databasepassword + "@tcp(" + dbcred.Host + ":" + dbcred.Port + ")/" + dbcred.Databasename
	}
	return nil
}

//InitDB initializes connection to the database
func InitDB() {

	//SET Afresh
	SetDBInfo()
	//fmt.Println(dataSourceName)
	if len(dataSourceName) > 10 {

		//fmt.Println(dataSourceName)
		var err error
		DB, err = sql.Open("mysql", dataSourceName)
		if err != nil {
			fmt.Println(err.Error())
			log.Panic(err)
			_ = blog.Botlog("Error Opening connection to Database : " + err.Error())
		}

		if err = DB.Ping(); err != nil {
			fmt.Println(err.Error())
			log.Panic(err)
			_ = blog.Botlog("Error, failed reaching database : " + err.Error())
		}
	}
}
